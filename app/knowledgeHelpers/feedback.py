from app import db
import json
from heapq import nlargest

def processFeedback(studentChoice):
	rawFeedback = json.load(open('./app/results/rawFeedback.txt'))

	finalFeedback = {}

	choicesMatch = isMatch(int(studentChoice['choiceStudent']), rawFeedback['preferredTherapy'])
	sdmFollowed = checkSDM(rawFeedback)
	questionsAsked = checkQuestionsAsked(rawFeedback['questionsAsked'])
	questionsGenerated = checkQuestionsGenerated(rawFeedback['generatedQuestions'])
	keywords = checkKeywords(rawFeedback['inputReceived'])
	perfectSession = checkPerfectSession(rawFeedback['perfectTherapyResults'])
	userSession = checkUserSession(rawFeedback['userTherapyResults'])
	desireSatisfaction = checkDesireSatisfaction(rawFeedback['desireSatisfactionResults'])
	prosAndConsMentioned = checkProsAndConsMentioned(rawFeedback['prosAndConsMentioned'])

	finalFeedback['choicesMatch'] = choicesMatch
	finalFeedback['sdmFollowed'] = sdmFollowed
	finalFeedback['questionsAsked'] = questionsAsked
	finalFeedback['questionsGenerated'] = questionsGenerated
	finalFeedback['keywords'] = keywords
	finalFeedback['perfectSession'] = perfectSession
	finalFeedback['userSession'] = userSession
	finalFeedback['desireSatisfaction'] = desireSatisfaction
	finalFeedback['prosAndConsMentioned'] = prosAndConsMentioned

	with open('./app/results/finalFeedback.json', 'w') as outfile:
		json.dump(finalFeedback, outfile)

	return finalFeedback

def checkProsAndConsMentioned(prosAndConsMentionedResults):
	prosAndConsMentionedFeedback = {}
	for key in prosAndConsMentionedResults:
		prosAndConsMentioned = prosAndConsMentionedResults[key]

		if prosAndConsMentioned == 'sufficient':
			prosAndConsMentionedFeedback[key] = 'Je hebt genoeg verteld over de voor- en nadelen van deze therapie.'
		elif prosAndConsMentioned == 'insufficient':
			prosAndConsMentionedFeedback[key] = 'Je had meer kunnen vertellen over de voor- en nadelen van deze therapie.'
		else:
			prosAndConsMentionedFeedback[key] = 'Je hebt deze therapie niet duidelijk genoeg benoemd.'

	return prosAndConsMentionedFeedback

def isMatch(studentChoice, vpChoice):
	return studentChoice == vpChoice

def checkDesireSatisfaction(desireSatisfactionResults):
	desireSatisfactionFeedback = {}

	desireSatisfaction = {}
	desireCount = {}

	for key in desireSatisfactionResults:
		tid,did,eid,importance = key.split("-")
		freq = desireSatisfactionResults[key]

		desireSatisfaction[did] = int(importance)

		# Check if the desire has been mentioned and if it is not "geen therapie"
		if desireSatisfactionResults[key] is not 0:
			if int(tid) is not 1:
				if did in desireCount:
					desireCount[did] += freq
				else:
					desireCount[did] = freq

	# Calculate average
	average = sum(desireSatisfaction.values()) / len(desireSatisfaction)

	# Amount above average
	numberOfImportantDesires = 0
	for key in desireSatisfaction:
		if desireSatisfaction[key] > average:
			numberOfImportantDesires += 1

	mostTalkedAboutDesires = nlargest(numberOfImportantDesires, desireCount, key=desireCount.get)
	mostImportantDesires = nlargest(numberOfImportantDesires, desireSatisfaction, key=desireSatisfaction.get)

	# Quick and Dirty :(
	desires = {}
	desires['1'] = ' Kans op overleven'
	desires['2'] = ' Kans op pijn'
	desires['3'] = ' Kans op terugkomen van de kanker'
	desires['4'] = ' Kans op ongemak'
	desires['5'] = ' Vrijheid om te kunnen blijven bewegen'
	desires['6'] = ' Vrijheid om te kunnen blijven werken'
	desires['7'] = ' Vrijheid om te kunnen blijven sporten'
	desires['8'] = ' Kans op verandering van uiterlijk'
	desires['9'] = ' Kans op onvruchtbaarheid'
	desires['10'] = ' Kans op kwijtraken libido'

	mostImportantFeedback = {key:desires[key] for key in mostImportantDesires if key in desires}
	mostTalkedAboutFeedback = {key:desires[key] for key in mostTalkedAboutDesires if key in desires}

	desireSatisfactionFeedback['mostImportantDesires'] = mostImportantFeedback.values()
	desireSatisfactionFeedback['mostTalkedAboutDesires'] = mostTalkedAboutFeedback.values()

	return desireSatisfactionFeedback

def checkUserSession(userTherapyResults):
	userSessionFeedback = {}
	for therapy in userTherapyResults:
		therapyData = userTherapyResults[therapy]
		
		therapyId = int(therapyData['therapyId'])
		therapyImpact = therapyData['therapyImpact']

		userSessionFeedback[therapyId] = therapyImpact

	return userSessionFeedback

def checkPerfectSession(perfectTherapyResults):
	perfectSessionFeedback = {}
	for therapy in perfectTherapyResults:
		therapyData = perfectTherapyResults[therapy]
		
		therapyId = int(therapyData['therapyId'])
		therapyImpact = therapyData['therapyImpact']

		perfectSessionFeedback[therapyId] = therapyImpact

	return perfectSessionFeedback

def checkKeywords(inputReceived):
	inputFeedback = {}
	for inputSentence in inputReceived:
		inputData = inputReceived[inputSentence]

		message = inputData['message']
		keywords = inputData['keywords'].split(',')

		for key in keywords:
			if key in inputFeedback:
				inputFeedback[key] += 1
			else:
				inputFeedback[key] = 1

	return inputFeedback

def	checkQuestionsGenerated(questionsGenerated):
	questionFeedback = {}
	for question in questionsGenerated:
		qData = questionsGenerated[question]

		askedDid = qData['askedDid']
		askedTid = qData['askedTid']
		generatedQuestionType = qData['generatedQuestionType']

		key = askedTid + '-' + askedDid + '-' + generatedQuestionType
		if key in questionFeedback:
			questionFeedback[key] += 1
		else:
			questionFeedback[key] = 1

	return questionFeedback

def checkQuestionsAsked(questionsAsked):
	questionFeedback = {}
	for question in questionsAsked:
		qData = questionsAsked[question]

		questionType = qData['questionType']
		questionSubject = qData['questionSubject']

		key = questionType + '-' + questionSubject
		if key in questionFeedback:
			questionFeedback[key] += 1
		else:
			questionFeedback[key] = 1

	return questionFeedback

def checkSDM(rawFeedback):
	sdmFeedback = {}

	# 1: sdmMentioned
	sdmMentioned = rawFeedback['sdmMentioned']
	sdmMentionedRemark = ''
	if not sdmMentioned:
		sdmMentionedRemark = 'Je bent vergeten gezamenlijke besluitvorming duidelijk te benoemen.'
	else:
		sdmMentionedRemark = 'Je hebt duidelijk verteld dat het gesprek gaat over gezamenlijke besluitvorming.'

	sdmFeedback['step1'] = {'sdmMentioned': sdmMentioned, 'sdmMentionedRemark': sdmMentionedRemark}

	# 2: prosAndConsMentioned
	prosAndConsMentioned = rawFeedback['prosAndConsMentioned']
	informedSufficient = False
	prosAndConsMentionedRemark = ''
	if not sdmMentioned:
		if (prosAndConsMentioned['1'] == 'sufficient' and prosAndConsMentioned['2'] == 'sufficient'):
			informedSufficient = True
			prosAndConsMentionedRemark = 'Je hebt duidelijk verteld over de voor- en nadelen van de beschikbare therapien, maar bent vergeten gezamenlijke besluitvorming duidelijk te benoemen.'
		else:
			prosAndConsMentionedRemark = 'Je hebt niet genoeg verteld over de verschillende voor en nadelen van de beschikbare therapien.'
	else:
		if (prosAndConsMentioned['3'] == 'sufficient' and prosAndConsMentioned['4'] == 'sufficient'):
			informedSufficient = True
			prosAndConsMentionedRemark = 'Je hebt duidelijk verteld over de voor- en nadelen van de beschikbare therapien.'
		else:
			prosAndConsMentionedRemark = 'Je hebt niet genoeg verteld over de verschillende voor en nadelen van de beschikbare therapien.'

	sdmFeedback['step2'] = {'informedSufficient': informedSufficient, 'prosAndConsMentionedRemark': prosAndConsMentionedRemark}

	# 3: preferencesAsked
	preferencesAsked = rawFeedback['preferencesAsked']
	preferencesAskedRemark = ''
	if not preferencesAsked:
		preferencesAskedRemark = 'Je bent vergeten duidelijk te vragen naar de voorkeuren van de patient.'
	else:
		preferencesAskedRemark = 'Je hebt duidelijk gevraagd naar de voorkeuren van de patient.'

	sdmFeedback['step3'] = {'preferencesAsked': preferencesAsked, 'preferencesAskedRemark': preferencesAskedRemark}

	# 4: decisionMade
	decisionMade = rawFeedback['decisionMade']
	decisionMadeRemark = ''
	if not decisionMade:
		decisionMadeRemark = 'Je bent vergeten duidelijk de keuze te verifieren.'
	else:
		decisionMadeRemark = 'Je hebt samen met de patient een keuze gemaakt.'

	sdmFeedback['step4'] = {'decisionMade': decisionMade, 'decisionMadeRemark': decisionMadeRemark}

	return sdmFeedback
