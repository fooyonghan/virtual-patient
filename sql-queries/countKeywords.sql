SELECT 	virtual_patient.input.keywords,
		COUNT(*) AS 'count'
FROM	virtual_patient.conversation
	LEFT JOIN virtual_patient.input
		ON virtual_patient.conversation.message = virtual_patient.input.message
	WHERE virtual_patient.conversation.source='user' AND virtual_patient.input.keywords IS NOT NULL
GROUP BY virtual_patient.input.keywords