SELECT effect_on_desire.desireId, desire.desireName, SUM(effect_on_desire.patientIsInformed) as sum
FROM effect_on_desire
LEFT JOIN desire ON desire.id=effect_on_desire.desireId
GROUP BY effect_on_desire.desireId