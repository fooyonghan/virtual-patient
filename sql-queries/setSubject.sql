INSERT INTO virtual_patient.history (subject)
SELECT DISTINCT virtual_patient.history.subject
FROM virtual_patient.history
JOIN virtual_patient.therapy
ON virtual_patient.therapy.therapyName = virtual_patient.history.subject
WHERE virtual_patient.therapy.id = 3