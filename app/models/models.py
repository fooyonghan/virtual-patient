from app import db
from datetime import datetime
# Database model, to create db:

# . venv/bin/activate
# python
# from app import db
# db.create_all()

class Therapy(db.Model):
	__tablename__ = 'therapy'
	id = db.Column('id', db.Integer, primary_key=True)
	therapyName = db.Column('therapyName', db.String(255))
	isRelevant = db.Column('isRelevant', db.Integer)
	nameKnown = db.Column('nameKnown', db.Boolean, default=False, nullable=False)
	effects = db.relationship('EffectOnTherapy', back_populates='therapy')

class Desire(db.Model):
	__tablename__ = 'desire'
	id = db.Column('id', db.Integer, primary_key=True)
	desireName = db.Column('desireName', db.String(255))
	importanceToPatient = db.Column('importanceToPatient', db.Integer)
	effects = db.relationship('EffectOnDesire', back_populates='desire')

class Effect(db.Model):
	__tablename__ = 'effect'
	id = db.Column('id', db.Integer, primary_key=True)
	effectName = db.Column('effectName', db.String(255))
	desires = db.relationship('EffectOnDesire', back_populates='effect')
	therapies = db.relationship('EffectOnTherapy', back_populates='effect')

# Association objects
class EffectOnTherapy(db.Model):
	__tablename__ = 'effect_on_therapy'
	therapyId = db.Column(db.Integer, db.ForeignKey('therapy.id'), primary_key=True)
	effectId = db.Column(db.Integer, db.ForeignKey('effect.id'), primary_key=True)
	frequency = db.Column(db.Integer, default=0)
	impact = db.Column(db.Integer)
	therapy = db.relationship("Therapy", back_populates="effects")
	effect = db.relationship("Effect", back_populates="therapies")

class EffectOnDesire(db.Model):
	__tablename__ = 'effect_on_desire'
	desireId = db.Column(db.Integer, db.ForeignKey('desire.id'), primary_key=True)
	effectId = db.Column(db.Integer, db.ForeignKey('effect.id'), primary_key=True)
	desire = db.relationship("Desire", back_populates="effects")
	effect = db.relationship("Effect", back_populates="desires")

class Input(db.Model):
	__tablename__ = 'input'
	id = db.Column('id', db.Integer, primary_key=True)
	message = db.Column('message', db.String(1000))
	keywords = db.Column('keywords', db.String(1000))
	dateTime = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

class Question(db.Model):
	__tablename__ = 'question'
	id = db.Column('id', db.Integer, primary_key=True)
	question = db.Column('question', db.String(1000))
	questionType = db.Column('questionType', db.String(1000))
	questionSubject = db.Column('questionSubject', db.String(1000))
	dateTime = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

class Response(db.Model):
	__tablename__ = 'response'
	id = db.Column('id', db.Integer, primary_key=True)
	response = db.Column('response', db.String(1000))
	responseType = db.Column('responseType', db.String(1000))
	dateTime = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

class GeneratedQuestion(db.Model):
	__tablename__ = 'generated_question'
	id = db.Column('id', db.Integer, primary_key=True)
	generatedQuestion = db.Column('generatedQuestion', db.String(1000))
	askedTid = db.Column('askedTid', db.Integer)
	askedDid = db.Column('askedDid', db.Integer)
	generatedQuestionType = db.Column('generatedQuestionType', db.String(1000))
	dateTime = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

class Conversation(db.Model):
	__tablename__ = 'conversation'
	id = db.Column('id', db.Integer, primary_key=True)
	message = db.Column('message', db.String(1000))
	source = db.Column('source', db.String(1000))
	dateTime = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

class History(db.Model):
	__tablename__ = 'history'
	id = db.Column('id', db.Integer, primary_key=True)
	subject = db.Column('subject', db.String(1000))
	dateTime = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

	def __init__(self, subject):
		self.subject = subject

def initHistory():
	db.session.add(History('someTherapy'))
	db.session.commit()

def resetDB():
	db.engine.execute("SET FOREIGN_KEY_CHECKS=0;")
	db.drop_all()
	db.engine.execute("SET FOREIGN_KEY_CHECKS=1;")
	db.create_all()