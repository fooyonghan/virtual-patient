SELECT 	virtual_patient.conversation.id,
		virtual_patient.conversation.dateTime,
		virtual_patient.conversation.message,
		virtual_patient.conversation.source,
        virtual_patient.question.questionSubject,
        virtual_patient.input.keywords
FROM	virtual_patient.conversation
	LEFT JOIN virtual_patient.question
		ON virtual_patient.conversation.message = virtual_patient.question.question
	LEFT JOIN virtual_patient.input
		ON virtual_patient.conversation.message = virtual_patient.input.message
WHERE virtual_patient.question.questionSubject IS NOT NULL
	OR virtual_patient.input.keywords = 'shareddecisionmaking'