from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO

UPLOAD_FOLDER = './uploads'

# App configurations
app = Flask(__name__)
app.config['SECRET_KEY'] = 'supersecret'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

# socketio runs as 'layer' around the original app
socketio = SocketIO(app, threaded=True)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:cthulhuCAT?33@localhost/virtual_patient'
db = SQLAlchemy(app)

from app import views
import models.models
import knowledgeHelpers