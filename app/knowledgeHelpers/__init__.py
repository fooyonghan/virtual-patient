# Package with everything that is related to analysing and modifying the knowledge entered
# by the user and interpreted by the virtual patient.
#  - knowledge.py: transforms and updates user input to knowledge in the db
#  - parser.py: simple input parser
#  - responseGenerator.py: matches data /knowledge from the java client with the corresponding response message
# 		stored in the uploaded virtual patient json file.
#  - feedback.py: generates feedback from the knowledge / conversation model in the database.