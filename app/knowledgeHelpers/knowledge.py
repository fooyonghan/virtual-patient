from app import db
from sets import Set
from sqlalchemy import and_, or_, not_, exists
from parser import *
from ..models.models import *
import json, ast

# Update knowledge in the database
def updateKnowledge(keywords):
	setCurrentSubject(keywords)
	
	try:
		currentSubject = db.session.query(History.subject).order_by(History.id.desc()).first()
		currentSubject = currentSubject[0].encode("utf-8")
		currentSubject = list(currentSubject.split(','))[0]
		
		therapyId = db.session.query(Therapy.id).filter(Therapy.therapyName==currentSubject).first()[0]

		keywords = removeWords(keywords, subjects)
		for k in keywords:
			effectId = db.session.query(Effect.id).filter(Effect.effectName==k).first()[0]
			effectOnTherapyMatch = db.session.query(EffectOnTherapy).filter(and_(EffectOnTherapy.therapyId==therapyId,EffectOnTherapy.effectId==effectId)).first()

			if effectOnTherapyMatch is not None:
				effectOnTherapyMatch.frequency += 1

			db.session.commit()

	except TypeError:
		print("Oh no, nothing relevant has been said yet!")

# Change the current subject of the conversation
def setCurrentSubject(keywords):
	keywords = keywords.split(',')
	currentSubject = set(subjects).intersection(keywords)

	if len(currentSubject) != 0:
		currentSubject = list(currentSubject)
		currentSubject = ','.join(currentSubject)

		# If History does not contain SDM yet, history = someTherapy
		sdmMentioned = db.session.query(exists().where(History.subject=="shareddecisionmaking")).scalar()
		if(not(sdmMentioned) and (currentSubject!="shareddecisionmaking")):
			currentSubject = "someTherapy"

		db.session.add(History(subject=currentSubject))
		db.session.commit()

# When a question is generated, update the current subject
def updateCurrentSubjectWhenQuestionsIsAsked(did, tid):
	currentSubject = ""

	if did == "0":
		return

	if tid == "2":
		currentSubject = "someTherapy"
	elif tid == "3":
		currentSubject = "hormoneTherapy"
	elif tid == "4":
		currentSubject = "chemoTherapy"
	else:
		return

	print currentSubject

	db.session.add(History(subject=currentSubject))
	db.session.commit()

# NOT USED?!
def updateTherapyKnowledge():
	someTherapy = db.session.query(Therapy).filter(Therapy.therapyName=='someTherapy').first()
	print someTherapy
	someTherapy.isRelevant = 0

	hormoneTherapy = db.session.query(Therapy).filter(Therapy.therapyName=='hormoneTherapy').first()
	hormoneTherapy.isRelevant = 1

	chemoTherapy = db.session.query(Therapy).filter(Therapy.therapyName=='chemoTherapy').first()
	chemoTherapy.isRelevant = 1

	db.session.commit()

