from app import db
from .models import Desire, Effect, EffectOnDesire
import os, os.path,sys, json
from pprint import pprint

#	Fill desires for patient
##	Which desires are important for patient
def initDesires():
	basedir = os.path.abspath(os.path.dirname(__file__))
	filename = "../uploads/chosenPatient.json"
	data = json.load(open(os.path.join(basedir, filename)))

	db.session.add(		Desire(desireName='lowChanceOfDeath',
		importanceToPatient=data["desires"]["1"]["importanceToPatient"]						))
	db.session.add(		Desire(desireName='lowChanceOfPain', 
		importanceToPatient=data["desires"]["2"]["importanceToPatient"]						))
	db.session.add(		Desire(desireName='lowChanceOfRecurrence', 
		importanceToPatient=data["desires"]["3"]["importanceToPatient"]						))
	db.session.add(		Desire(desireName='lowChanceOfDiscomfort', 
		importanceToPatient=data["desires"]["4"]["importanceToPatient"]						))
	db.session.add(		Desire(desireName='freedomToMove', 
		importanceToPatient=data["desires"]["5"]["importanceToPatient"]						))
	db.session.add(		Desire(desireName='freedomToWork', 
		importanceToPatient=data["desires"]["6"]["importanceToPatient"]						))
	db.session.add(		Desire(desireName='freedomToSport', 
		importanceToPatient=data["desires"]["7"]["importanceToPatient"]						))
	db.session.add(		Desire(desireName='lowChanceOfChangeInAesthetic', 
		importanceToPatient=data["desires"]["8"]["importanceToPatient"]						))
	db.session.add(		Desire(desireName='lowChanceOfInfertility', 
		importanceToPatient=data["desires"]["9"]["importanceToPatient"]						))
	db.session.add(		Desire(desireName='lowSexDrive', 
		importanceToPatient=data["desires"]["10"]["importanceToPatient"]					))

	db.session.commit()


##	Which desires are influenced by therapy effects
def bindDesireAndEffects():
	lowChanceOfDeath = db.session.query(Desire).filter(Desire.desireName=='lowChanceOfDeath').first()
	lowChanceOfPain = db.session.query(Desire).filter(Desire.desireName=='lowChanceOfPain').first()
	lowChanceOfRecurrence = db.session.query(Desire).filter(Desire.desireName=='lowChanceOfRecurrence').first()
	lowChanceOfDiscomfort = db.session.query(Desire).filter(Desire.desireName=='lowChanceOfDiscomfort').first()
	freedomToMove = db.session.query(Desire).filter(Desire.desireName=='freedomToMove').first()
	freedomToWork = db.session.query(Desire).filter(Desire.desireName=='freedomToWork').first()
	freedomToSport = db.session.query(Desire).filter(Desire.desireName=='freedomToSport').first()
	lowChanceOfChangeInAesthetic = db.session.query(Desire).filter(Desire.desireName=='lowChanceOfChangeInAesthetic').first()
	lowChanceOfInfertility = db.session.query(Desire).filter(Desire.desireName=='lowChanceOfInfertility').first()
	lowSexDrive = db.session.query(Desire).filter(Desire.desireName=='lowSexDrive').first()

	lowerChanceOfDeath = db.session.query(Effect).filter(Effect.effectName=='lowerChanceOfDeath').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfDeath.id, effectId=lowerChanceOfDeath.id))

	lowerChanceOfRecurrence = db.session.query(Effect).filter(Effect.effectName=='lowerChanceOfRecurrence').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfRecurrence.id, effectId=lowerChanceOfRecurrence.id))

	cognitiveFunction = db.session.query(Effect).filter(Effect.effectName=='cognitiveFunction').first()
	db.session.add(EffectOnDesire(desireId=freedomToMove.id, effectId=cognitiveFunction.id))
	db.session.add(EffectOnDesire(desireId=freedomToWork.id, effectId=cognitiveFunction.id))
	db.session.add(EffectOnDesire(desireId=freedomToSport.id, effectId=cognitiveFunction.id))

	earlyMenopause = db.session.query(Effect).filter(Effect.effectName=='earlyMenopause').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfPain.id, effectId=earlyMenopause.id))
	db.session.add(EffectOnDesire(desireId=lowChanceOfInfertility.id, effectId=earlyMenopause.id))
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=earlyMenopause.id))

	chanceOfInfertility = db.session.query(Effect).filter(Effect.effectName=='chanceOfInfertility').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfInfertility.id, effectId=chanceOfInfertility.id))
	
	fatigue = db.session.query(Effect).filter(Effect.effectName=='fatigue').first()
	db.session.add(EffectOnDesire(desireId=freedomToMove.id, effectId=fatigue.id))
	db.session.add(EffectOnDesire(desireId=freedomToWork.id, effectId=fatigue.id))
	db.session.add(EffectOnDesire(desireId=freedomToSport.id, effectId=fatigue.id))
	db.session.add(EffectOnDesire(desireId=lowSexDrive.id, effectId=fatigue.id))

	hairLoss = db.session.query(Effect).filter(Effect.effectName=='hairLoss').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfChangeInAesthetic.id, effectId=hairLoss.id))

	hotFlash = db.session.query(Effect).filter(Effect.effectName=='hotFlash').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=hotFlash.id))

	jointPain = db.session.query(Effect).filter(Effect.effectName=='jointPain').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfPain.id, effectId=jointPain.id))
	db.session.add(EffectOnDesire(desireId=freedomToMove.id, effectId=jointPain.id))
	db.session.add(EffectOnDesire(desireId=freedomToWork.id, effectId=jointPain.id))
	db.session.add(EffectOnDesire(desireId=freedomToSport.id, effectId=jointPain.id))
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=jointPain.id))

	lossBoneDensity = db.session.query(Effect).filter(Effect.effectName=='lossBoneDensity').first()
	db.session.add(EffectOnDesire(desireId=freedomToMove.id, effectId=lossBoneDensity.id))
	db.session.add(EffectOnDesire(desireId=freedomToSport.id, effectId=lossBoneDensity.id))

	lossSexDrive = db.session.query(Effect).filter(Effect.effectName=='lossSexDrive').first()
	db.session.add(EffectOnDesire(desireId=lowSexDrive.id, effectId=lossSexDrive.id))

	mouthSores = db.session.query(Effect).filter(Effect.effectName=='mouthSores').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfPain.id, effectId=mouthSores.id))
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=mouthSores.id))

	musclePain = db.session.query(Effect).filter(Effect.effectName=='musclePain').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfPain.id, effectId=musclePain.id))
	db.session.add(EffectOnDesire(desireId=freedomToMove.id, effectId=musclePain.id))
	db.session.add(EffectOnDesire(desireId=freedomToSport.id, effectId=musclePain.id))
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=musclePain.id))

	nailWeakness = db.session.query(Effect).filter(Effect.effectName=='nailWeakness').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfChangeInAesthetic.id, effectId=nailWeakness.id))

	nausea = db.session.query(Effect).filter(Effect.effectName=='nausea').first()
	db.session.add(EffectOnDesire(desireId=freedomToMove.id, effectId=nausea.id))
	db.session.add(EffectOnDesire(desireId=freedomToWork.id, effectId=nausea.id))
	db.session.add(EffectOnDesire(desireId=freedomToSport.id, effectId=nausea.id))
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=nausea.id))

	nervePain = db.session.query(Effect).filter(Effect.effectName=='nervePain').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfPain.id, effectId=nervePain.id))
	db.session.add(EffectOnDesire(desireId=freedomToMove.id, effectId=nervePain.id))
	db.session.add(EffectOnDesire(desireId=freedomToWork.id, effectId=nervePain.id))
	db.session.add(EffectOnDesire(desireId=freedomToSport.id, effectId=nervePain.id))
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=nervePain.id))

	nightSweat = db.session.query(Effect).filter(Effect.effectName=='nightSweat').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=nightSweat.id))

	vaginalDryness = db.session.query(Effect).filter(Effect.effectName=='vaginalDryness').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=vaginalDryness.id))
	db.session.add(EffectOnDesire(desireId=lowSexDrive.id, effectId=vaginalDryness.id))

	weightGain = db.session.query(Effect).filter(Effect.effectName=='weightGain').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfChangeInAesthetic.id, effectId=weightGain.id))


	chanceOfPain = db.session.query(Effect).filter(Effect.effectName=='chanceOfPain').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfPain.id, effectId=chanceOfPain.id))

	chanceOfDiscomfort = db.session.query(Effect).filter(Effect.effectName=='chanceOfDiscomfort').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfDiscomfort.id, effectId=chanceOfDiscomfort.id))

	chanceToMove = db.session.query(Effect).filter(Effect.effectName=='chanceToMove').first()
	db.session.add(EffectOnDesire(desireId=freedomToMove.id, effectId=chanceToMove.id))

	chanceToWork = db.session.query(Effect).filter(Effect.effectName=='chanceToWork').first()
	db.session.add(EffectOnDesire(desireId=freedomToWork.id, effectId=chanceToWork.id))
	
	chanceToSport = db.session.query(Effect).filter(Effect.effectName=='chanceToSport').first()
	db.session.add(EffectOnDesire(desireId=freedomToSport.id, effectId=chanceToSport.id))

	chanceToKeepAesthetic = db.session.query(Effect).filter(Effect.effectName=='chanceToKeepAesthetic').first()
	db.session.add(EffectOnDesire(desireId=lowChanceOfChangeInAesthetic.id, effectId=chanceToKeepAesthetic.id))

	db.session.commit()
