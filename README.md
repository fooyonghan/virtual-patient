Master thesis project. A virtual patient that focuses on teaching shared decision making to medical students.

This repo contains the backend and user client. Below a high level view of the application can be seen.

![](/images/vp-architecture.png)

The architecture consist of Flask as a webserver, a MySQL database to store responses, a Unity client through which users could
interact with the virtual patient and a combination of DialogFlow and Jadex as the reasoning component.

![](/images/vp-cognitive-layer.png)

At its core the project builds a knowledge base, based on user input and returns responses using the BDI paradigm (beliefs,
desires and intentions).

The BDI engine can be found here:
https://gitlab.com/fooyonghan/virtual-patient-bdi

If you are really interested and got a canteen full of coffee, you can read the whole thesis document here:
https://repository.tudelft.nl/islandora/object/uuid:7f5d8e12-bd75-4d1f-aba5-324103fe0420?collection=education