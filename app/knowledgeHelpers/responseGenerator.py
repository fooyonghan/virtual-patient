import random, string
from app import db
from knowledge import updateCurrentSubjectWhenQuestionsIsAsked
from ..models.models import GeneratedQuestion

def generateAnswer(response, data):
	askedQuestionType, askedQuestionSubject = response.split(",")

	if askedQuestionType == 'unknown':
		askedQuestionSubject, responseValue = askedQuestionSubject.split(";")
		answers = data["unknown-answer"]["answers"]
		return random.choice(answers) + ". Wat ik wel belangrijk vind is, " + random.choice(data["desires"][responseValue]["answers"])
	if askedQuestionType == 'chitchat':
		answers = data["chit-chat-answer"][askedQuestionSubject]["answers"]
		return random.choice(answers)
	elif askedQuestionType == 'profile':
		answers = data["profile-answer"][askedQuestionSubject]["answers"]
		return random.choice(answers)
	elif askedQuestionType == 'knowledge':
		return generateKnowledgeAnswer(askedQuestionSubject, data)
	else:
		return "Wat bedoelt U precies?"

def generateKnowledgeAnswer(askedQuestionSubject, data):
	knowledgeAnswers = data["knowledge-answer"]
	askedQuestionSubject, responseValue = askedQuestionSubject.split(";")

	if askedQuestionSubject == "choicePossible":
		if responseValue == "true":
			return random.choice(knowledgeAnswers["choicePossible"]["possible"])
		else:
			return random.choice(knowledgeAnswers["choicePossible"]["notPossible"])
	elif askedQuestionSubject == "therapyPreference":
		if responseValue == "0":
			return random.choice(knowledgeAnswers["choicePossible"]["notPossible"])
		else:
			answer = random.choice(knowledgeAnswers["therapyPreference"]["answers"])
			tName = random.choice(data["therapies"][responseValue]["response"])
			
			# Replace tid in the answer with the correct therapy name
			answer = string.replace(answer, '<tid>', tName)
			return answer
	elif askedQuestionSubject == "desireSatisfaction":
		did, tid = responseValue.split("+")
		test = did + ',' + tid
		reply = generateQuestion(test, data)
		return reply
	elif askedQuestionSubject == "desires":
		# Return top most important desires
		return random.choice(data["desires"][responseValue]["answers"])
	elif askedQuestionSubject == "conversationPurpose":
		if responseValue == "getKnowledge":
			return random.choice(knowledgeAnswers["conversationPurpose"]["getKnowledge"])
		else:
			return random.choice(knowledgeAnswers["conversationPurpose"]["sharedDecisionMaking"])

			# Code could be more compact if I keep the strings consistent:
			# 	return random.choice(knowledgeAnswers["conversationPurpose"][responseValue])
	elif askedQuestionSubject == "chemo":
		return random.choice(knowledgeAnswers["chemo"][responseValue])
	elif askedQuestionSubject == "chemoChoice":
		return random.choice(knowledgeAnswers["chemoChoice"][responseValue])
	elif askedQuestionSubject == "hormones":
		return random.choice(knowledgeAnswers["hormones"][responseValue])
	elif askedQuestionSubject == "hormonesChoice":
		return random.choice(knowledgeAnswers["hormonesChoice"][responseValue])
	else:
		print "OOPSIE unknown questionSubject"

# Generate a generic response when new information has been given
def generateGenericResponse(data):
	genericConfirmingRemarks = data["generic-confirming-remarks"]
	genericConfirmingRemark = random.choice(genericConfirmingRemarks)

	return genericConfirmingRemark

#  Generate a question from therapy and desire on mind
#  Save reaction to db
def generateQuestion(ids, data):
	askedDid, askedTid = ids.split(",")

	# # Update last talked about subject in history
	# db.session.add(History(subject = unknownTherapyQuestion))
	# db.session.commit()
	updateCurrentSubjectWhenQuestionsIsAsked(askedDid, askedTid)

	# Return a remark when general information is given
	if askedDid == "0" and askedTid == "0":
		genericConfirmingRemarks = data["generic-confirming-remarks"]
		genericConfirmingRemark = random.choice(genericConfirmingRemarks)

		db.session.add(GeneratedQuestion(generatedQuestion = genericConfirmingRemark, askedTid = askedTid, askedDid = askedDid, generatedQuestionType = "genericConfirmingRemark"))
		db.session.commit()
		return genericConfirmingRemark

	# Return a question when SDM is triggered but the therapy names are unknown
	if askedDid == "0" and askedTid != "0":
		unknownTherapyQuestions = data["unknown-therapy-questions"]
		unknownTherapyQuestion = random.choice(unknownTherapyQuestions)

		db.session.add(GeneratedQuestion(generatedQuestion = unknownTherapyQuestion, askedTid = askedTid, askedDid = askedDid, generatedQuestionType = "unknownTherapyQuestion"))
		db.session.commit()
		return unknownTherapyQuestion

	#  Return a remark about therapy knowledge satisfaction
	#  TODO better check
	if askedDid == "11" and askedTid == "11":
		knowledgeSatisfiedResponses = data["therapy-knowledge-satisfied"]
		knowledgeSatisfiedResponse = random.choice(knowledgeSatisfiedResponses)

		db.session.add(GeneratedQuestion(generatedQuestion = knowledgeSatisfiedResponse, askedTid = askedTid, askedDid = askedDid, generatedQuestionType = "knowledgeSatisfiedResponse"))
		db.session.commit()
		return knowledgeSatisfiedResponse

	# Return a question about a therapy and its effect on a desire
	dAnswer = random.choice(data["desires"][askedDid]["questions"])
	tAnswer = random.choice(data["therapies"][askedTid]["response"])

	replyStruct = random.choice(data["question-reply-structures"])
	replyStruct = string.replace(replyStruct, '<did>', dAnswer)
	replyStruct = string.replace(replyStruct, '<tid>', tAnswer)

	db.session.add(GeneratedQuestion(generatedQuestion = replyStruct, askedTid = askedTid, askedDid = askedDid, generatedQuestionType = "knowledgeQuestion"))
	db.session.commit()
	return replyStruct