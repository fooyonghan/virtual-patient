// Little bit of javascript to handle user input and virtual patient communication, including:
// - TalkingCoach communication
// - buttons
// - webkitspeech

$(document).ready(function(){
	var socket = io.connect('http://127.0.0.1:5000');

	// -------- Hit TalkingCoach API --------
	// Make the unity virtual patient speak the given msg
	socket.on('speakMessage', function(msg) {
		console.log('message', msg);
		SendMessage('TalkingCoach', 'convertToSpeach', msg);
	});

	// -------- Button inputs --------
	$('#inputButton').on('click', function() {
		var input = $('#myInput').val()

		if (input.includes("?")) {
			socket.emit('newQuestion', $('#myInput').val());
			$('#myInput').val('');
		} else {
			socket.emit('newInput', $('#myInput').val());
			$('#myInput').val('');
		}
	});

	// Button for speech input
	$('#infobutton').on('click', function() {
		socket.emit('newInput', $('#myInfo').val());
		$('#myInfo').val('');
	});

	// Send on Enter
	$('#myInput').on('keyup', function (e) {
		if (e.keyCode == 13) {
			var input = $('#myInput').val()

			if (input.includes("?")) {
				socket.emit('newQuestion', $('#myInput').val());
				$('#myInput').val('');
			} else {
				socket.emit('newInput', $('#myInput').val());
				$('#myInput').val('');
			}
		}
	});

	// Reset timer to say something when someone is typing
	$('#myInput').keyup(function() {
    	socket.emit('typingQuestion');
	});

	// End conversation
	$('#stopbutton').on('click', function() {
		var answer = confirm('Wil je echt stoppen?');
		
		if(answer){
			socket.emit('endConversation');
			window.location.href = '/question'
		}
		else{
			e.preventDefault();
		}
	});

	// -------- Speech recognition ---------
	// Setup
	var recognition = new webkitSpeechRecognition();
	var recognizing = false;

	recognition.onstart = function () {
		recognizing = true;
	};

	recognition.onend = function () {
		recognizing = false;
	};

	recognition.onerror = function (event) {
		recognizing = false;
	};

	recognition.continuous = false;
	recognition.interimResults = false;
	recognition.lang = "nl-nl";

	// Store user speech input in the database every timerInterval
	$('#speechbutton').on('click', function() {
		console.log("Starting speech input!");

		setInterval(function(){
			if (!recognizing) {
				recognition.start();
			}

			recognition.onresult = function(event) {
				document.getElementById('myInfo').value = event.results[0][0].transcript;
				recognition.stop();
				jQuery('#infobutton').click();
			}

			delay(function(){
				recognition.stop();
			}, 4500 );
		}, 5000);
	});

	var delay = ( function() {
		var timer = 0;
		return function(callback, ms) {
			clearTimeout (timer);
			timer = setTimeout(callback, ms);
		};
	})();

});