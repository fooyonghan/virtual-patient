UPDATE virtual_patient.therapy t2 JOIN virtual_patient.therapy t3 JOIN virtual_patient.therapy t4
    ON t2.id = 2 AND t3.id = 3 AND t4.id = 4
   SET t2.isRelevant = 0,
       t3.isRelevant = 1,
       t4.isRelevant = 1;