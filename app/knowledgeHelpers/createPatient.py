# Create a new patient and assign new desire values to it.
# The patient needs to have at least 3 values above the average
# so that it will ask about those desires.

import random
import os, os.path,sys, json

desireValues = []

for x in xrange(0,10):
	randomNumber = random.randint(1, 100)
	desireValues.append(randomNumber)

average = sum(desireValues) / float(len(desireValues))

desiresAboveAverage = 0
for d in desireValues:
	if d > average:
		desiresAboveAverage += 1

if desiresAboveAverage is 3 or desiresAboveAverage is 4:
	basedir = os.path.abspath(os.path.dirname(__file__))
	filename = "../uploads/chosenPatient.json"
	data = json.load(open(os.path.join(basedir, filename)))

	data["desires"]["1"]["importanceToPatient"] = desireValues[0]
	data["desires"]["2"]["importanceToPatient"] = desireValues[1]
	data["desires"]["3"]["importanceToPatient"] = desireValues[2]
	data["desires"]["4"]["importanceToPatient"] = desireValues[3]
	data["desires"]["5"]["importanceToPatient"] = desireValues[4]
	data["desires"]["6"]["importanceToPatient"] = desireValues[5]
	data["desires"]["7"]["importanceToPatient"] = desireValues[6]
	data["desires"]["8"]["importanceToPatient"] = desireValues[7]
	data["desires"]["9"]["importanceToPatient"] = desireValues[8]
	data["desires"]["10"]["importanceToPatient"] = desireValues[9]

	jsonFile = open("../uploads/chosenPatient.json", "w+")
	jsonFile.write(json.dumps(data, sort_keys=True, indent=4))
	jsonFile.close()

	print(desiresAboveAverage)
	print(desireValues)
	print(average)
else:
	print("Not enough desires above the average")
	# print(desiresAboveAverage)
	# print(desireValues)
	# print(average)