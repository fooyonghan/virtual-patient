from app import app, socketio, db
from flask import render_template, Flask, request, redirect, url_for
from flask_socketio import SocketIO, send, emit
from werkzeug.utils import secure_filename
from pprint import pprint
import os, os.path,sys, json

from models.models import Input, Question, resetDB, initHistory, Response, Conversation
from models.therapy import initTherapy, initEffects, bindTherapyAndEffects
from models.desire import initDesires, bindDesireAndEffects
from knowledgeHelpers.parser import removeWords, stop
from knowledgeHelpers.knowledge import updateKnowledge, setCurrentSubject
from knowledgeHelpers.responseGenerator import generateQuestion, generateAnswer, generateGenericResponse
from knowledgeHelpers.feedback import processFeedback

try:
	import apiai
except ImportError:
	sys.path.append(
		os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
	)
	import apiai

CLIENT_ACCESS_TOKEN = '4f45e68fdfcf44b58d201cec4d6ed518'
ALLOWED_EXTENSIONS = set(['txt', 'json'])

# -------- Server communication --------
# SocketIO is used for communication with the java client and app.js.
# It keeps a pipeline constantly open, so that there is no necessity
# for page reloads and http requests during communication with the
# virtual patient.

# Get a response that matches your msg from APIAI / DialogFlow
@socketio.on('callAPIAI')
def callAPIAI(msg):
	ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)

	request = ai.text_request()
	request.lang = 'nl'
	request.session_id = "<SESSION ID, UNIQUE FOR EACH USER>"
	request.query = msg
	response = json.loads(request.getresponse().read().decode('utf-8'))

	return response

# Save input to db and notify other clients of new message
@socketio.on('newInput')
def newInput(msg):
	# Store in db
	db.session.add(Conversation(message=msg, source="user"))
	db.session.commit()

	parsedMsg = removeWords(msg, stop)
	keywords = set()

	for word in parsedMsg:
		response = callAPIAI(word)
		if "knowledge.update" in response['result']['metadata']['intentName']:
			updatedKnowledge = response['result']['fulfillment']['messages'][1]['payload']
			keywords.add(updatedKnowledge['effect'])
			# Catch chit chat words


	keywords = list(keywords)
	keywords = ','.join(keywords)

	db.session.add(Input(message=msg, keywords=keywords))
	db.session.commit()

	updateKnowledge(keywords)
	emit('newInput', msg, broadcast=True)

	# If some relevant things have been said
	if len(keywords) is not 0:
		# Speak: generic-confirming-remark (okay, hmmhmm, right)
		basedir = os.path.abspath(os.path.dirname(__file__))
		filename = "uploads/chosenPatient.json"
		data = json.load(open(os.path.join(basedir, filename)))

		emit('speakMessage', generateGenericResponse(data), broadcast=True)
	# If no relevant keywords were said
	else:
		# Speak: response that matches DialogFlow
		print "Nothing relevant has been said"

		# ----------------------- TURN OFF FOR ONLY SPEECH ------------------------
		chitchat(msg)

# Store a question asked in the db and answer it by responding with the virtual patient.
@socketio.on('newQuestion')
def newQuestion(msg):
	# Store in db
	db.session.add(Conversation(message=msg, source="user"))
	db.session.commit()
	
	response = callAPIAI(msg)
	print json.dumps(response, indent=4, sort_keys=True)
	
	basedir = os.path.abspath(os.path.dirname(__file__))
	filename = "uploads/chosenPatient.json"
	data = json.load(open(os.path.join(basedir, filename)))

	if response['result']['action'] == 'input.unknown':
		db.session.add(Question(question=msg, questionType="unknown", questionSubject="unknown"))
		db.session.commit()
			
		emit('newQuestion', msg, broadcast=True)
	else:
		answer = response['result']['fulfillment']['messages'][0]['payload']
		questionType = answer['questionType']
		questionSubject = answer['questionSubject']

		if questionType == 'knowledge':
			# Check db for answer
			# Emit update "Question asked" to Java Client, include questionType and questionSubject
			db.session.add(Question(question=msg, questionType=questionType, questionSubject=questionSubject))
			db.session.commit()

			emit('newQuestion', msg, broadcast=True)
		elif questionType == 'profile' or questionType == 'chitchat':
			# Get answer from database regarding this message
			db.session.add(Question(question=msg, questionType=questionType, questionSubject=questionSubject))
			db.session.commit()

			response = questionType + ',' + questionSubject
			answer = generateAnswer(response, data)

			emit('speakMessage', answer, broadcast=True)
		else:
			print "No known questionType"

# When a question is being typed, withhold the java client from making responses.
@socketio.on('typingQuestion')
def typingQuestion():
	emit('typingQuestion', broadcast=True)

# Return a chit chat response when nothing relevant has been said
# DEPRECATED?
@socketio.on('chitchat')
def chitchat(msg):
	try:
		response = callAPIAI(msg)
		print response
		answer = response['result']['fulfillment']['speech']
		emit('speakMessage', answer, broadcast=True)
	except KeyError:
		print "No input was given"
	
# Make the virtual patient speak
@socketio.on('speakMessage')
def speakMessage(msg):
	print "This is the message: "
	print msg

	# Get answer from database regarding this message
	basedir = os.path.abspath(os.path.dirname(__file__))
	filename = "uploads/chosenPatient.json"
	data = json.load(open(os.path.join(basedir, filename)))
	responseType, response = msg.split("-")

	if responseType == "question":
		question = generateQuestion(response, data)

		# Save to database
		db.session.add(Conversation(message=question, source="virtual_patient"))
		db.session.commit()

		emit('speakMessage', question, broadcast=True)
	elif responseType == "answer":
		answer = generateAnswer(response, data)

		# Save to database
		db.session.add(Conversation(message=answer, source="virtual_patient"))
		db.session.commit()

		emit('speakMessage', answer, broadcast=True)

# Call to java client to calculate feedback and store it in results folder when the conersation is ended.
@socketio.on('endConversation')
def endConversation():
	emit('endConversation', broadcast=True)

# -------- App routes --------
@app.route("/")
def main():
	return render_template('upload-patient.html')

# The virtual patient
@app.route("/virtual-patient")
def virtualPatient():
	return render_template('index.html')

# Upload a virtual patient
@app.route('/upload-patient', methods=['GET', 'POST'])
def upload_file():
	if request.method == 'POST':
		# check if the post request has the file part
		if 'file' not in request.files:
			flash('No file part')
			return redirect(request.url)
		file = request.files['file']
		# if user does not select file, browser also
		# submit a empty part without filename
		if file.filename == '':
			flash('No selected file')
			return redirect(request.url)
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			basedir = os.path.abspath(os.path.dirname(__file__))
			file.save(os.path.join(basedir, app.config['UPLOAD_FOLDER'], filename))

			# Populate database
			resetDB()
			initTherapy()
			initDesires()
			initEffects()
			initHistory()
			bindTherapyAndEffects()
			bindDesireAndEffects()

			return redirect(url_for('virtualPatient'))
	return render_template('upload-patient.html')

def allowed_file(filename):
		return '.' in filename and \
					 filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Final question about what choice was be made
@app.route("/question", methods=['GET', 'POST'])
def question():
	if request.method == 'POST':
		# Create feedback
		feedback = {}
		feedback['choiceStudent'] = request.form['therapie']

		processedFeedbackData = processFeedback(feedback)
		# print processedFeedbackData

		return render_template('feedback.html', processedFeedbackData=json.dumps(processedFeedbackData))
	if request.method == 'GET':
		return render_template('question.html')

# Feedback page
@app.route("/feedback")
def feedback():
	return render_template('feedback.html')

