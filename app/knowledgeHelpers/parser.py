import nltk
from nltk import word_tokenize
from nltk.corpus import stopwords
import json

stop = set(stopwords.words('dutch'))

subjects = [
	'chemotherapy',
	'hormonetherapy',
	'shareddecisionmaking'
]

def removeWords(input, stopwords):
	input = input.replace(",", " ")
	return [i for i in word_tokenize(input.lower()) if i not in stopwords]