SELECT	virtual_patient.effect_on_therapy.therapyId,
        virtual_patient.effect_on_therapy.frequency,
        virtual_patient.effect_on_therapy.impact,
        virtual_patient.effect_on_desire.effectId,
        virtual_patient.effect_on_desire.desireId,
		virtual_patient.desire.importanceToPatient,
        virtual_patient.therapy.isRelevant,
        virtual_patient.therapy.nameKnown
FROM virtual_patient.effect_on_desire
	JOIN virtual_patient.effect_on_therapy
		ON virtual_patient.effect_on_desire.effectId = virtual_patient.effect_on_therapy.effectId
	JOIN virtual_patient.therapy
		ON virtual_patient.effect_on_therapy.therapyId = virtual_patient.therapy.id
	JOIN virtual_patient.desire
		ON virtual_patient.desire.id = virtual_patient.effect_on_desire.desireId
	WHERE frequency<>0