from app import db
from .models import Therapy, Effect, EffectOnTherapy

def initTherapy():
	noTherapy = Therapy(		therapyName='noTherapy', isRelevant=1, nameKnown=True			)
	someTherapy = Therapy(		therapyName='someTherapy', isRelevant=1, nameKnown=True			)
	hormoneTherapy = Therapy(	therapyName='hormoneTherapy', isRelevant=0, nameKnown=False		)
	chemoTherapy = Therapy(		therapyName='chemoTherapy', isRelevant=0, nameKnown=False		)

	db.session.add(noTherapy)
	db.session.add(someTherapy)
	db.session.add(hormoneTherapy)
	db.session.add(chemoTherapy)

	db.session.commit()

def initEffects():
	db.session.add(Effect(	effectName='lowerChanceOfDeath'			))
	db.session.add(Effect(	effectName='lowerChanceOfRecurrence'	))
	db.session.add(Effect(	effectName='chanceOfInfertility'		))
	db.session.add(Effect(	effectName='hotFlash'					))
	db.session.add(Effect(	effectName='nightSweat'					))
	db.session.add(Effect(	effectName='jointPain'					))
	db.session.add(Effect(	effectName='musclePain'					))
	db.session.add(Effect(	effectName='lossBoneDensity'			))
	db.session.add(Effect(	effectName='lossSexDrive'				))
	db.session.add(Effect(	effectName='vaginalDryness'				))
	db.session.add(Effect(	effectName='hairLoss'					))
	db.session.add(Effect(	effectName='nausea'						))
	db.session.add(Effect(	effectName='nailWeakness'				))
	db.session.add(Effect(	effectName='nervePain'					))
	db.session.add(Effect(	effectName='mouthSores'					))
	db.session.add(Effect(	effectName='fatigue'					))
	db.session.add(Effect(	effectName='earlyMenopause'				))
	db.session.add(Effect(	effectName='weightGain'					))
	db.session.add(Effect(	effectName='cognitiveFunction'			))

	db.session.add(Effect(	effectName='chanceOfPain'				))
	db.session.add(Effect(	effectName='chanceOfDiscomfort'			))
	db.session.add(Effect(	effectName='chanceToMove'				))
	db.session.add(Effect(	effectName='chanceToWork'				))
	db.session.add(Effect(	effectName='chanceToSport'				))
	db.session.add(Effect(	effectName='chanceToKeepAesthetic'		))

	db.session.commit()

def bindTherapyAndEffects():
	noTherapy = db.session.query(Therapy).filter(Therapy.therapyName=='noTherapy').first()
	someTherapy = db.session.query(Therapy).filter(Therapy.therapyName=='someTherapy').first()
	hormoneTherapy = db.session.query(Therapy).filter(Therapy.therapyName=='hormoneTherapy').first()
	chemoTherapy = db.session.query(Therapy).filter(Therapy.therapyName=='chemoTherapy').first()

	lowerChanceOfDeath = db.session.query(Effect).filter(Effect.effectName=='lowerChanceOfDeath').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=lowerChanceOfDeath.id, frequency=5, impact=-10))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=lowerChanceOfDeath.id, impact=7))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=lowerChanceOfDeath.id, impact=7))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=lowerChanceOfDeath.id, impact=9))

	lowerChanceOfRecurrence = db.session.query(Effect).filter(Effect.effectName=='lowerChanceOfRecurrence').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=lowerChanceOfRecurrence.id, frequency=5, impact=-10))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=lowerChanceOfRecurrence.id, impact=5))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=lowerChanceOfRecurrence.id, impact=5))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=lowerChanceOfRecurrence.id, impact=5))

	chanceOfInfertility = db.session.query(Effect).filter(Effect.effectName=='chanceOfInfertility').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=chanceOfInfertility.id, frequency=5, impact=-10))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=chanceOfInfertility.id, impact=-8))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=chanceOfInfertility.id, impact=-8))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=chanceOfInfertility.id, impact=-4))

	cognitiveFunction = db.session.query(Effect).filter(Effect.effectName=='cognitiveFunction').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=cognitiveFunction.id, frequency=5, impact=-5))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=cognitiveFunction.id, impact=-3))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=cognitiveFunction.id, impact=0))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=cognitiveFunction.id, impact=-3))

	earlyMenopause = db.session.query(Effect).filter(Effect.effectName=='earlyMenopause').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=earlyMenopause.id, frequency=5, impact=0))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=earlyMenopause.id, impact=0))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=earlyMenopause.id, impact=-5))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=earlyMenopause.id, impact=0))

	fatigue = db.session.query(Effect).filter(Effect.effectName=='fatigue').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=fatigue.id, frequency=5, impact=-5))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=fatigue.id, impact=-6))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=fatigue.id, impact=-3))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=fatigue.id, impact=-6))

	hairLoss = db.session.query(Effect).filter(Effect.effectName=='hairLoss').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=hairLoss.id, frequency=5, impact=0))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=hairLoss.id, impact=-8))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=hairLoss.id, impact=0))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=hairLoss.id, impact=-8))

	hotFlash = db.session.query(Effect).filter(Effect.effectName=='hotFlash').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=hotFlash.id, frequency=5, impact=0))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=hotFlash.id, impact=-6))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=hotFlash.id, impact=-6))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=hotFlash.id, impact=-4))

	jointPain = db.session.query(Effect).filter(Effect.effectName=='jointPain').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=jointPain.id,frequency=5, impact=-6))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=jointPain.id, impact=-5))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=jointPain.id, impact=-5))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=jointPain.id, impact=0))

	lossBoneDensity = db.session.query(Effect).filter(Effect.effectName=='lossBoneDensity').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=lossBoneDensity.id, frequency=5, impact=0))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=lossBoneDensity.id, impact=-4))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=lossBoneDensity.id, impact=-4))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=lossBoneDensity.id, impact=0))

	lossSexDrive = db.session.query(Effect).filter(Effect.effectName=='lossSexDrive').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=lossSexDrive.id,frequency=5, impact=-2))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=lossSexDrive.id, impact=-4))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=lossSexDrive.id, impact=-4))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=lossSexDrive.id, impact=-4))

	mouthSores = db.session.query(Effect).filter(Effect.effectName=='mouthSores').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=mouthSores.id,frequency=5, impact=0))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=mouthSores.id, impact=-3))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=mouthSores.id, impact=0))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=mouthSores.id, impact=-3))

	musclePain = db.session.query(Effect).filter(Effect.effectName=='musclePain').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=musclePain.id,frequency=5, impact=-5))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=musclePain.id, impact=-5))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=musclePain.id, impact=-5))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=musclePain.id, impact=-7))

	nailWeakness = db.session.query(Effect).filter(Effect.effectName=='nailWeakness').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=nailWeakness.id,frequency=5, impact=0))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=nailWeakness.id, impact=-2))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=nailWeakness.id, impact=0))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=nailWeakness.id, impact=-2))

	nausea = db.session.query(Effect).filter(Effect.effectName=='nausea').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=nausea.id,frequency=5, impact=-4))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=nausea.id, impact=-8))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=nausea.id, impact=-2))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=nausea.id, impact=-8))

	nervePain = db.session.query(Effect).filter(Effect.effectName=='nervePain').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=nervePain.id,frequency=5, impact=-5))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=nervePain.id, impact=-7))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=nervePain.id, impact=0))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=nervePain.id, impact=-7))

	nightSweat = db.session.query(Effect).filter(Effect.effectName=='nightSweat').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=nightSweat.id,frequency=5, impact=-2))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=nightSweat.id, impact=-2))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=nightSweat.id, impact=-2))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=nightSweat.id, impact=0))

	vaginalDryness = db.session.query(Effect).filter(Effect.effectName=='vaginalDryness').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=vaginalDryness.id,frequency=5, impact=0))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=vaginalDryness.id, impact=-2))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=vaginalDryness.id, impact=-2))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=vaginalDryness.id, impact=0))

	weightGain = db.session.query(Effect).filter(Effect.effectName=='weightGain').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=weightGain.id,frequency=5, impact=0))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=weightGain.id, impact=-3))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=weightGain.id, impact=-3))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=weightGain.id, impact=0))



	chanceOfPain = db.session.query(Effect).filter(Effect.effectName=='chanceOfPain').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=chanceOfPain.id,frequency=5, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=chanceOfPain.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=chanceOfPain.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=chanceOfPain.id, impact=-1))

	chanceOfDiscomfort = db.session.query(Effect).filter(Effect.effectName=='chanceOfDiscomfort').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=chanceOfDiscomfort.id,frequency=5, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=chanceOfDiscomfort.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=chanceOfDiscomfort.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=chanceOfDiscomfort.id, impact=-1))
	
	chanceToMove = db.session.query(Effect).filter(Effect.effectName=='chanceToMove').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=chanceToMove.id,frequency=5, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=chanceToMove.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=chanceToMove.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=chanceToMove.id, impact=-1))

	chanceToWork = db.session.query(Effect).filter(Effect.effectName=='chanceToWork').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=chanceToWork.id,frequency=5, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=chanceToWork.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=chanceToWork.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=chanceToWork.id, impact=-1))

	chanceToSport = db.session.query(Effect).filter(Effect.effectName=='chanceToSport').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=chanceToSport.id,frequency=5, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=chanceToSport.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=chanceToSport.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=chanceToSport.id, impact=-1))

	chanceToKeepAesthetic = db.session.query(Effect).filter(Effect.effectName=='chanceToKeepAesthetic').first()
	db.session.add(EffectOnTherapy(therapyId=noTherapy.id, effectId=chanceToKeepAesthetic.id,frequency=5, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=someTherapy.id, effectId=chanceToKeepAesthetic.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=hormoneTherapy.id, effectId=chanceToKeepAesthetic.id, impact=-1))
	db.session.add(EffectOnTherapy(therapyId=chemoTherapy.id, effectId=chanceToKeepAesthetic.id, impact=-1))

	db.session.commit()











