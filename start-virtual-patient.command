#!/bin/bash
echo \(Re\)Starting database
mysql.server start
echo Starting virtual environment and flask server
mydir="$(dirname "$BASH_SOURCE")"
source "$mydir/venv/bin/activate"
python "$mydir/runserver.py"