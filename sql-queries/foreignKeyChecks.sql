SET FOREIGN_KEY_CHECKS=0;
drop table if exists desire;
drop table if exists effect;
drop table if exists effect_on_desire;
drop table if exists effect_on_therapy;
drop table if exists history;
drop table if exists input;
drop table if exists therapy;
SET FOREIGN_KEY_CHECKS=1;